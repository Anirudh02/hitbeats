<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
	
	.fixedheader{
		position:fixed;
		top:0;
		left:0;
		right:0;
		z-index:9;
		
		
	}
	.sec1{
		margin-left:250px;
	}
	.sec2{
		margin-left:250px;
	}
	.sec3{
		margin-left:250px;
	}
	.sec4{
		margin-left:250px;
	}
	.image{
		margin-left:110px;
	}
	#section1{
		background-color:#00ff7d6b;
	}
	#section2{
		background-color:#985b96ab;
	}
	#section3{
		background-color:#80000054;
	}
	#section4{
		background-color:#2196f399;
	}
	
	.footer{
		background-color:#000;
	}
	.footer ul{
		list-style-type:none;
	}
  body {
	  padding-top:100px;
      position: relative;
  }
  .affix {
      top: 20px;
      z-index: 9999 !important;
	
  }
  div.col-sm-9 div {
      height: 250px;
     font-size: 28px;
  }
  #myScrollspy .affix{top: 130px;}
  @media screen and (max-width: 810px) {
    #section1, #section2, #section3, #section4,  {
        margin-left: 150px;
    }
  }
  
/* Full-width input fields */
input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

/* Set a style for all buttons */
button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

/* Extra styles for the cancel button */
.cancelbtn {
    padding: 14px 20px;
    background-color: #f44336;
}

/* Float cancel and signup buttons and add an equal width */
.cancelbtn,.signupbtn {float:left;width:50%}

/* Add padding to container elements */
.container {
    padding: 16px;
}
	</style>
  </head>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="15">

<nav class="navbar navbar-inverse fixedheader">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">HITBEATS</a>
    </div>
    
	<div class="search col-md-offset-8">
    <form class="navbar-form navbar-left">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for songs & Artist">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit">
            <i class="glyphicon glyphicon-search"></i>
          </button>
        </div>
      </div>
    </form>
	</div>
	<div>
	<ul class="nav navbar-nav">
    <li><a href="#">Login</a></li>
	  <li><button type="button" class="btn" data-toggle="modal" data-target="#signup">Register</button>
	  </li>
     
    </ul>
	</div>
  </div>
</nav>


<div class="container-fluid">
  <div class="row">
    <nav class="col-sm-2" id="myScrollspy">
      <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="205">
        <li><a href="#section1">BOLLYWOOD</a></li>
        <li><a href="#section2">HOLLYWOOD</a></li>
        <li><a href="#section3">REMIX</a></li>
		 <li><a href="#section4">INSTRUMENTAL</a></li>
        
          </ul>
        </li>
      </ul>
    </nav>
    <div class="col-sm-7">
		<div id="section1">    
			
    <div class="row">
		<h2 class="col-md-12 sec1">BOLLYWOOD</h2>
	 </div>
	<div class="row ">
	    <div class="col-md-2 image">
			<a href="#" class="thumbnail">
                <img src="image/okjaanu.png">
             </a>
        </div>
		<div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/bajiraomastani.png">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/Aksar.png">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/kick.png">
                        </a>
         </div>
	 </div><!-- /row1 -->
	        <div class="row">
                <div class="col-md-2 image">
                        <a href="#" class="thumbnail">
                          <img src="image/sanamre.png">
                        </a>.
							
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/sanamterikasam.png">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/sultan.png">
                        </a>
                </div> <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/MS-Dhoni.png">
                        </a>
			
   </div>
				
				
	        </div><!-- /row2 -->
	    </div><!-- /section1 --><br>
	
      <div id="section2"> 
        <div class="row">
		<h2 class="col-md-12 sec2">HOLLYWOOD</h2>
	</div><!-- /row -->
	<div class="row">
	    <div class="col-md-2 image">
			<a href="#" class="thumbnail">
                <img src="image/arianagrande.jpg">
             </a>
        </div>
		<div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/chainsmokers.jpg">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/coldplay.jpg">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/edshareen.jpg">
                        </a>
         </div>
	 </div><!-- /row1 -->
	        <div class="row">
                <div class="col-md-2 image">
                        <a href="#" class="thumbnail">
                          <img src="image/edshareen.jpg">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/justinbeiber.jpg">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/shakira.jpg">
                        </a>
                </div>
				 <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="image/pitbul.jpg">
                        </a>
         </div>
				
	        </div><!-- /row2 -->
	    </div><!-- /section2 --><br>
	  <div id="section3">
        <div class="row">
		<h2 class="col-xs-12 sec3">REMIX</h2>
	</div><!-- /row -->
	<div class="row">
	    <div class="col-md-2 image">
			<a href="#" class="thumbnail">
                <img src="//placehold.it/200X200">
             </a>
        </div>
		<div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
	 </div><!-- /row1 -->
	        <div class="row">
                <div class="col-md-2 image">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
                </div>
				 <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
				
	        </div><!-- /row2 -->
	    </div><!-- /section3 --><br>
	  <div id="section4">
        <div class="row">
		<h2 class="col-xs-12 sec4">INSTRUMENTAL</h2>
	</div><!-- /row -->
	<div class="row">
	    <div class="col-md-2 image">
			<a href="#" class="thumbnail">
                <img src="//placehold.it/200X200">
             </a>
        </div>
		<div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
         <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
	 </div><!-- /row1 -->
	        <div class="row">
                <div class="col-md-2 image">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
                </div>
                <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
                </div>
				 <div class="col-md-2">
                        <a href="#" class="thumbnail">
                          <img src="//placehold.it/200x200">
                        </a>
         </div>
	        </div><!-- /row2 -->
	    </div><!-- /section4 --><br>
	</div>


	  <div class="col-sm-3" id="myScrollspy">
      <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="205">
        <li><a href="#section">PLAY NOW</a></li>
        <li><marquee scrollamount="8" direction="right"  behaviour="alternate">
     DESPACITO
	 </marquee></li>
	<li> <audio controls>
  <source src="songs/Despacito.mp3">
  </li></audio>
        
          </ul>
        
      
    
    </div>
 </div>
 </div>

<!--Footer-->

    <!--Footer Links-->
    <div  class="container-fluid footer">
        <div class="row">

            <!--First column-->
            <div class="col-md-8">
                <h5 class="title">Footer Content</h5>
                <p>Here you can use rows and columns here to organize your footer content.</p>
            </div>
            <!--/.First column-->

            <!--Second column-->
            <div class="col-md-4">
                <h5 class="title">Links</h5>
                <ul >
                    <li><a href="#!"><img src="fb.png"></a></li>
                   <li><a href="#!"><img src="twitter.png"></a></li>
                    <li><a href="#!"><img src="insta.png"></a></li>
                    
                </ul>
            </div>
            <!--/.Second column-->
        </div>
    
    <!--/.Footer Links-->

    <!--Copyright-->
   
	</div>
    <!--/.Copyright-->
<!--/.Footer-->
<div id="signup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form action="" method="">
			Email:<input type="email" name="email">
			
				</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>